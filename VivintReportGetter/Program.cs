﻿// This code was written by Marth141 for the purpose of getting work at a company done.
// I can gurantee there are no company secrets hidden in this code. What you see is what there is.
// 10/28/2016

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using iTextSharp.text;

namespace VivintReportGetter
{
    class Program
    {
        // The main program. This accepts only one file with the string[] args thing there.
        static void Main(string[] args)
        {
            // Here a variable looked for is declared as a stringbuilder object.
            var prod = new StringBuilder();

            // I think this is to make sure a file is there and that it exists.
            if (args.Length > 0 && File.Exists(args[0]))
            {
                string pdfDocument = args[0]; // the file, shown as args[0] is loaded as pdfDocument.
                // The PdfReader object implements IDisposable.Dispose, so you can
                // wrap it in the using keyword to automatically dispose of it
                using (var pdfReader = new PdfReader(pdfDocument)) // pdfReader, a part of iTextSharp, begins reading the document.
                {
                    // Loop through each page of the document
                    for (var pageNumber = 3; pageNumber <= pdfReader.NumberOfPages; pageNumber++)
                    {
                        // This Nuget package, iTextSharp, looks for a region with this distanceinfrom...
                        // being a distant in from sides of the page. Like an x, y point.
                        float distanceInPixelsFromLeft = 500;
                        float distanceInPixelsFromBottom = 490;
                        // Once the origin point is stored above,
                        // These two lines define a box area for capture.
                        float width = 50;
                        float height = 60;

                        // Here the box area is made a rectangle variable using a rectangle method.
                        var rect = new System.util.RectangleJ(
                            distanceInPixelsFromLeft,
                            distanceInPixelsFromBottom,
                            width,
                            height);

                        // Here a new filters variable is made using iTextSharp renderfilter method.
                        var filters = new RenderFilter[1];
                        // Here the filter array takes in the rectangle region.
                        filters[0] = new RegionTextRenderFilter(rect);

                        // Here the iTextsharp usage is worked using it's listener and strategy methods taking the filtered area.
                        ITextExtractionStrategy strategy =
                            new FilteredTextRenderListener(
                                new LocationTextExtractionStrategy(),
                                filters);

                        // Here the extrator method is used calling it's gettextfrompage bit,
                        // I think that is a combination of a few different things iTextSharp is
                        // doing to get text into the memory and variable.
                        var currentText = PdfTextExtractor.GetTextFromPage(
                            pdfReader,
                            pageNumber,
                            strategy);

                        // Here I think it's taking the text that is available and encoding it into something usable.
                        // PDF is somewhat like an image and somewhat not so I suppose this is to make sure that
                        // what it's reading is text and can be made into something usable by the C# application.
                        currentText =
                                                Encoding.UTF8.GetString(Encoding.Convert(
                                                    Encoding.Default,
                                                    Encoding.UTF8,
                                                    Encoding.Default.GetBytes(currentText)));

                        // Here the application appends each hit from location search engine
                        // by making each find have a return carriage.
                        // This is because the iTextSharp program combines everything found as one string.
                        prod.Append(currentText + "\n");
                    }
                }
            }

            // This code was helped by a user on Stack Overflow.
            // His contribution was the productions.Length > 0 ? productions[0] : string.Empty; bit.
            // Before hand I could get a single read to show up, but while checking the debugger I could see
            // Multiple were being grabbed but just weren't displaying because of the one variable mess.
            // Wanting it to show in multiple variables for individual usage later, I attempted breaking it
            // into an array because I'm not sure how to do a functioning list.
            // His contribution allowed all of the variables for each bit of the array to be usable and displayed.
            // A translator would be nice. I understand enough to know that the variables are being defined as input
            // of the iTextSharp's original variable, in this case it was var prod.
            // var prod is stored as a string prodFinished after being converted to a string.
            // It's then put into the array and at each return carriage, it is split. The splits
            // are all stored in the variables numbered then displayed in the console window to verify they are
            // formatted correctly.
            string prodFinished = prod.ToString();
            string[] productions = prodFinished.Split('\n');
            string productions1 = productions.Length > 0 ? productions[0] : string.Empty;
            string productions2 = productions.Length > 1 ? productions[1] : string.Empty;
            string productions3 = productions.Length > 2 ? productions[2] : string.Empty;
            string productions4 = productions.Length > 3 ? productions[3] : string.Empty;
            string productions5 = productions.Length > 4 ? productions[4] : string.Empty;
            string productions6 = productions.Length > 5 ? productions[5] : string.Empty;
            string productions7 = productions.Length > 6 ? productions[6] : string.Empty;
            string productions8 = productions.Length > 7 ? productions[7] : string.Empty;
            string productions9 = productions.Length > 8 ? productions[8] : string.Empty;
            string productions10 = productions.Length > 9 ? productions[9] : string.Empty;
            string productions11 = productions.Length > 10 ? productions[10] : string.Empty;
            string productions12 = productions.Length > 11 ? productions[11] : string.Empty;
            string productions13 = productions.Length > 12 ? productions[12] : string.Empty;
            string productions14 = productions.Length > 13 ? productions[13] : string.Empty;
            string productions15 = productions.Length > 14 ? productions[14] : string.Empty;
            string productions16 = productions.Length > 15 ? productions[15] : string.Empty;
            string productions17 = productions.Length > 16 ? productions[16] : string.Empty;
            string productions18 = productions.Length > 17 ? productions[17] : string.Empty;
            string productions19 = productions.Length > 18 ? productions[18] : string.Empty;
            /* Console.WriteLine(productions1);
            Console.WriteLine(productions2);
            Console.WriteLine(productions3);
            Console.WriteLine(productions4);
            Console.WriteLine(productions5);
            Console.WriteLine(productions6);
            Console.WriteLine(productions7);
            Console.WriteLine(productions8);
            Console.WriteLine(productions9); */


            // This code and a lot of the code below is a straight copy and paste of the above
            // iTextSharp method and storing into variables. The difference is the information being grabbed.
            // In this case I did not want productions and wanted tilts, and vice versa.
            var tilt = new StringBuilder();

            if (args.Length > 0 && File.Exists(args[0]))
            {
                string pdfDocument = args[0];
                // The PdfReader object implements IDisposable.Dispose, so you can
                // wrap it in the using keyword to automatically dispose of it
                using (var pdfReader = new PdfReader(pdfDocument))
                {
                    // Loop through each page of the document
                    for (var pageNumber = 3; pageNumber <= pdfReader.NumberOfPages; pageNumber++)
                    {
                        float distanceInPixelsFromLeft = 90;
                        float distanceInPixelsFromBottom = 465;
                        float width = 500;
                        float height = 10;

                        var rect = new System.util.RectangleJ(
                            distanceInPixelsFromLeft,
                            distanceInPixelsFromBottom,
                            width,
                            height);

                        var filters = new RenderFilter[1];
                        filters[0] = new RegionTextRenderFilter(rect);

                        ITextExtractionStrategy strategy =
                            new FilteredTextRenderListener(
                                new LocationTextExtractionStrategy(),
                                filters);

                        var currentText = PdfTextExtractor.GetTextFromPage(
                            pdfReader,
                            pageNumber,
                            strategy);

                        currentText =
                                                Encoding.UTF8.GetString(Encoding.Convert(
                                                    Encoding.Default,
                                                    Encoding.UTF8,
                                                    Encoding.Default.GetBytes(currentText)));

                        tilt.Append(currentText + "\n");
                    }
                }
            }

            //You'll do something else with it, here I write it to a console window
            string tiltFinished = tilt.ToString();
            string[] tilts = tiltFinished.Replace("Slope: ", "").Split('\n');
            string tilts1 = tilts.Length > 0 ? tilts[0] : string.Empty;
            string tilts2 = tilts.Length > 1 ? tilts[1] : string.Empty;
            string tilts3 = tilts.Length > 2 ? tilts[2] : string.Empty;
            string tilts4 = tilts.Length > 3 ? tilts[3] : string.Empty;
            string tilts5 = tilts.Length > 4 ? tilts[4] : string.Empty;
            string tilts6 = tilts.Length > 5 ? tilts[5] : string.Empty;
            string tilts7 = tilts.Length > 6 ? tilts[6] : string.Empty;
            string tilts8 = tilts.Length > 7 ? tilts[7] : string.Empty;
            string tilts9 = tilts.Length > 8 ? tilts[8] : string.Empty;
            string tilts10 = tilts.Length > 9 ? tilts[9] : string.Empty;
            string tilts11 = tilts.Length > 10 ? tilts[10] : string.Empty;
            string tilts12 = tilts.Length > 11 ? tilts[11] : string.Empty;
            string tilts13 = tilts.Length > 12 ? tilts[12] : string.Empty;
            string tilts14 = tilts.Length > 13 ? tilts[13] : string.Empty;
            string tilts15 = tilts.Length > 14 ? tilts[14] : string.Empty;
            string tilts16 = tilts.Length > 15 ? tilts[15] : string.Empty;
            string tilts17 = tilts.Length > 16 ? tilts[16] : string.Empty;
            string tilts18 = tilts.Length > 17 ? tilts[17] : string.Empty;
            string tilts19 = tilts.Length > 18 ? tilts[18] : string.Empty;
            /* Console.WriteLine(tilts1.Replace(".00 °", ""));
            Console.WriteLine(tilts2.Replace(".00 °", ""));
            Console.WriteLine(tilts3.Replace(".00 °", ""));
            Console.WriteLine(tilts4.Replace(".00 °", ""));
            Console.WriteLine(tilts5.Replace(".00 °", ""));
            Console.WriteLine(tilts6.Replace(".00 °", ""));
            Console.WriteLine(tilts7.Replace(".00 °", ""));
            Console.WriteLine(tilts8.Replace(".00 °", ""));
            Console.WriteLine(tilts9.Replace(".00 °", "")); */


            var azimuth = new StringBuilder();

            if (args.Length > 0 && File.Exists(args[0]))
            {
                string pdfDocument = args[0];
                // The PdfReader object implements IDisposable.Dispose, so you can
                // wrap it in the using keyword to automatically dispose of it
                using (var pdfReader = new PdfReader(pdfDocument))
                {
                    // Loop through each page of the document
                    for (var pageNumber = 3; pageNumber <= pdfReader.NumberOfPages; pageNumber++)
                    {
                        float distanceInPixelsFromLeft = 80;
                        float distanceInPixelsFromBottom = 470;
                        float width = 500;
                        float height = 10;

                        var rect = new System.util.RectangleJ(
                            distanceInPixelsFromLeft,
                            distanceInPixelsFromBottom,
                            width,
                            height);

                        var filters = new RenderFilter[1];
                        filters[0] = new RegionTextRenderFilter(rect);

                        ITextExtractionStrategy strategy =
                            new FilteredTextRenderListener(
                                new LocationTextExtractionStrategy(),
                                filters);

                        var currentText = PdfTextExtractor.GetTextFromPage(
                            pdfReader,
                            pageNumber,
                            strategy);

                        currentText =
                                                Encoding.UTF8.GetString(Encoding.Convert(
                                                    Encoding.Default,
                                                    Encoding.UTF8,
                                                    Encoding.Default.GetBytes(currentText)));

                        azimuth.Append(currentText + "\n");
                    }
                }
            }

            //You'll do something else with it, here I write it to a console window
            string azimuthFinished = azimuth.ToString();
            string[] azimuths = azimuthFinished.Replace("Azimuth: ", "").Split('\n');
            string azimuths1 = azimuths.Length > 0 ? azimuths[0] : string.Empty;
            string azimuths2 = azimuths.Length > 1 ? azimuths[1] : string.Empty;
            string azimuths3 = azimuths.Length > 2 ? azimuths[2] : string.Empty;
            string azimuths4 = azimuths.Length > 3 ? azimuths[3] : string.Empty;
            string azimuths5 = azimuths.Length > 4 ? azimuths[4] : string.Empty;
            string azimuths6 = azimuths.Length > 5 ? azimuths[5] : string.Empty;
            string azimuths7 = azimuths.Length > 6 ? azimuths[6] : string.Empty;
            string azimuths8 = azimuths.Length > 7 ? azimuths[7] : string.Empty;
            string azimuths9 = azimuths.Length > 8 ? azimuths[8] : string.Empty;
            string azimuths10 = azimuths.Length > 9 ? azimuths[9] : string.Empty;
            string azimuths11 = azimuths.Length > 10 ? azimuths[10] : string.Empty;
            string azimuths12 = azimuths.Length > 11 ? azimuths[11] : string.Empty;
            string azimuths13 = azimuths.Length > 12 ? azimuths[12] : string.Empty;
            string azimuths14 = azimuths.Length > 13 ? azimuths[13] : string.Empty;
            string azimuths15 = azimuths.Length > 14 ? azimuths[14] : string.Empty;
            string azimuths16 = azimuths.Length > 15 ? azimuths[15] : string.Empty;
            string azimuths17 = azimuths.Length > 16 ? azimuths[16] : string.Empty;
            string azimuths18 = azimuths.Length > 17 ? azimuths[17] : string.Empty;
            string azimuths19 = azimuths.Length > 18 ? azimuths[18] : string.Empty;
            /* Console.WriteLine(azimuths1.Replace(" °", ""));
            Console.WriteLine(azimuths2.Replace(" °", ""));
            Console.WriteLine(azimuths3.Replace(" °", ""));
            Console.WriteLine(azimuths4.Replace(" °", ""));
            Console.WriteLine(azimuths5.Replace(" °", ""));
            Console.WriteLine(azimuths6.Replace(" °", ""));
            Console.WriteLine(azimuths7.Replace(" °", ""));
            Console.WriteLine(azimuths8.Replace(" °", ""));
            Console.WriteLine(azimuths9.Replace(" °", "")); */

            var module = new StringBuilder();

            if (args.Length > 0 && File.Exists(args[0]))
            {
                string pdfDocument = args[0];
                // The PdfReader object implements IDisposable.Dispose, so you can
                // wrap it in the using keyword to automatically dispose of it
                using (var pdfReader = new PdfReader(pdfDocument))
                {
                    // Loop through each page of the document
                    for (var pageNumber = 3; pageNumber <= pdfReader.NumberOfPages; pageNumber++)
                    {
                        float distanceInPixelsFromLeft = 80;
                        float distanceInPixelsFromBottom = 455;
                        float width = 500;
                        float height = 10;

                        var rect = new System.util.RectangleJ(
                            distanceInPixelsFromLeft,
                            distanceInPixelsFromBottom,
                            width,
                            height);

                        var filters = new RenderFilter[1];
                        filters[0] = new RegionTextRenderFilter(rect);

                        ITextExtractionStrategy strategy =
                            new FilteredTextRenderListener(
                                new LocationTextExtractionStrategy(),
                                filters);

                        var currentText = PdfTextExtractor.GetTextFromPage(
                            pdfReader,
                            pageNumber,
                            strategy);

                        currentText =
                                                Encoding.UTF8.GetString(Encoding.Convert(
                                                    Encoding.Default,
                                                    Encoding.UTF8,
                                                    Encoding.Default.GetBytes(currentText)));

                        module.Append(currentText + "\n");
                    }
                }
            }

            //You'll do something else with it, here I write it to a console window
            string modulesFinished = module.ToString();
            string[] modules = modulesFinished.Replace("Total Modules: ", "").Split('\n');
            string modules1 = modules.Length > 0 ? modules[0] : string.Empty;
            string modules2 = modules.Length > 1 ? modules[1] : string.Empty;
            string modules3 = modules.Length > 2 ? modules[2] : string.Empty;
            string modules4 = modules.Length > 3 ? modules[3] : string.Empty;
            string modules5 = modules.Length > 4 ? modules[4] : string.Empty;
            string modules6 = modules.Length > 5 ? modules[5] : string.Empty;
            string modules7 = modules.Length > 6 ? modules[6] : string.Empty;
            string modules8 = modules.Length > 7 ? modules[7] : string.Empty;
            string modules9 = modules.Length > 8 ? modules[8] : string.Empty;
            string modules10 = modules.Length > 9 ? modules[9] : string.Empty;
            string modules11 = modules.Length > 10 ? modules[10] : string.Empty;
            string modules12 = modules.Length > 11 ? modules[11] : string.Empty;
            string modules13 = modules.Length > 12 ? modules[12] : string.Empty;
            string modules14 = modules.Length > 13 ? modules[13] : string.Empty;
            string modules15 = modules.Length > 14 ? modules[14] : string.Empty;
            string modules16 = modules.Length > 15 ? modules[15] : string.Empty;
            string modules17 = modules.Length > 16 ? modules[16] : string.Empty;
            string modules18 = modules.Length > 17 ? modules[17] : string.Empty;
            string modules19 = modules.Length > 18 ? modules[18] : string.Empty;
            /* Console.WriteLine(modules1);//.Replace(" °", ""));
            Console.WriteLine(modules2);//.Replace(" °", ""));
            Console.WriteLine(modules3);//.Replace(" °", ""));
            Console.WriteLine(modules4);//.Replace(" °", ""));
            Console.WriteLine(modules5);//.Replace(" °", ""));
            Console.WriteLine(modules6);//.Replace(" °", ""));
            Console.WriteLine(modules7);//.Replace(" °", ""));
            Console.WriteLine(modules8);//.Replace(" °", ""));
            Console.WriteLine(modules9);//.Replace(" °", "")); */

            // Console.WriteLine(modulesFinished);

            // Here the program now creates an ini file to be used by an AutoHotKey application written by
            // Daniel Scholle. This is to be able to have Daniel's application have all of it's fields filled out.
            // The application stores it's fields in an ini file for memory. By having my application here
            // fill out that ini, I'm kind of "Save Editing" his tool to be able to have it ready to use
            // without the need for the user to fill out its information by hand. Saving the user time with data entry.
            string ini = ($@"[Main]
SNumber=19
Util=
Meter=
MDisc=
MBusb=
DownNumber=17
Rating=0.280
Efficiency=0.85
StateFull=
StateAB=
SystemMinimum=900
SectionMinimum=900
AR=3214567
FirstName=Jamie
LastName=Doe
Address=123 W Fake Street
City=Nowhere
Zip=00000
TextBox=
TotalsBox=
GoalMin=9001
Annual=10000
RoofSection1Box=1
RoofSection1E=
RoofSection1T={tilts1.Replace(".00 °", "")}
RoofSection1A={azimuths1.Replace(" °", "")}
RoofSection1M={modules1}
RoofSection1P={productions1}
RoofSection1S=
RoofSection1F=
RoofSection1N=
RoofSection2Box=1
RoofSection2E=
RoofSection2T={tilts2.Replace(".00 °", "")}
RoofSection2A={azimuths2.Replace(" °", "")}
RoofSection2M={modules2}
RoofSection2P={productions2}
RoofSection2S=
RoofSection2F=
RoofSection2N=
RoofSection3Box=1
RoofSection3E=
RoofSection3T={tilts3.Replace(".00 °", "")}
RoofSection3A={azimuths3.Replace(" °", "")}
RoofSection3M={modules3}
RoofSection3P={productions3}
RoofSection3S=
RoofSection3F=
RoofSection3N=
RoofSection4Box=1
RoofSection4E=
RoofSection4T={tilts4.Replace(".00 °", "")}
RoofSection4A={azimuths4.Replace(" °", "")}
RoofSection4M={modules4}
RoofSection4P={productions4}
RoofSection4S=
RoofSection4F=
RoofSection4N=
RoofSection5Box=1
RoofSection5E=
RoofSection5T={tilts5.Replace(".00 °", "")}
RoofSection5A={azimuths5.Replace(" °", "")}
RoofSection5M={modules5}
RoofSection5P={productions5}
RoofSection5S=
RoofSection5F=
RoofSection5N=
RoofSection6Box=1
RoofSection6E=
RoofSection6T={tilts6.Replace(".00 °", "")}
RoofSection6A={azimuths6.Replace(" °", "")}
RoofSection6M={modules6}
RoofSection6P={productions6}
RoofSection6S=
RoofSection6F=
RoofSection6N=
RoofSection7Box=1
RoofSection7E=
RoofSection7T={tilts7.Replace(".00 °", "")}
RoofSection7A={azimuths7.Replace(" °", "")}
RoofSection7M={modules7}
RoofSection7P={productions7}
RoofSection7S=
RoofSection7F=
RoofSection7N=
RoofSection8Box=1
RoofSection8E=
RoofSection8T={tilts8.Replace(".00 °", "")}
RoofSection8A={azimuths8.Replace(" °", "")}
RoofSection8M={modules8}
RoofSection8P={productions8}
RoofSection8S=
RoofSection8F=
RoofSection8N=
RoofSection9Box=1
RoofSection9E=
RoofSection9T={tilts9.Replace(".00 °", "")}
RoofSection9A={azimuths9.Replace(" °", "")}
RoofSection9M={modules9}
RoofSection9P={productions9}
RoofSection9S=
RoofSection9F=
RoofSection9N=
RoofSection10Box=1
RoofSection10E=
RoofSection10T={tilts10.Replace(".00 °", "")}
RoofSection10A={azimuths10.Replace(" °", "")}
RoofSection10M={modules10}
RoofSection10P={productions10}
RoofSection10S=
RoofSection10F=
RoofSection10N=
RoofSection11Box=1
RoofSection11E=
RoofSection11T={tilts11.Replace(".00 °", "")}
RoofSection11A={azimuths11.Replace(" °", "")}
RoofSection11M={modules11}
RoofSection11P={productions11}
RoofSection11S=
RoofSection11F=
RoofSection11N=
RoofSection12Box=1
RoofSection12E=
RoofSection12T={tilts12.Replace(".00 °", "")}
RoofSection12A={azimuths12.Replace(" °", "")}
RoofSection12M={modules12}
RoofSection12P={productions12}
RoofSection12S=
RoofSection12F=
RoofSection12N=
RoofSection13Box=1
RoofSection13E=
RoofSection13T={tilts13.Replace(".00 °", "")}
RoofSection13A={azimuths13.Replace(" °", "")}
RoofSection13M={modules13}
RoofSection13P={productions13}
RoofSection13S=
RoofSection13F=
RoofSection13N=
RoofSection14Box=1
RoofSection14E=
RoofSection14T={tilts14.Replace(".00 °", "")}
RoofSection14A={azimuths14.Replace(" °", "")}
RoofSection14M={modules14}
RoofSection14P={productions14}
RoofSection14S=
RoofSection14F=
RoofSection14N=
RoofSection15Box=1
RoofSection15E=
RoofSection15T={tilts15.Replace(".00 °", "")}
RoofSection15A={azimuths15.Replace(" °", "")}
RoofSection15M={modules15}
RoofSection15P={productions15}
RoofSection15S=
RoofSection15F=
RoofSection15N=
RoofSection16Box=1
RoofSection16E=
RoofSection16T={tilts16.Replace(".00 °", "")}
RoofSection16A={azimuths16.Replace(" °", "")}
RoofSection16M={modules16}
RoofSection16P={productions16}
RoofSection16S=
RoofSection16F=
RoofSection16N=
RoofSection17Box=1
RoofSection17E=
RoofSection17T={tilts17.Replace(".00 °", "")}
RoofSection17A={azimuths17.Replace(" °", "")}
RoofSection17M={modules17}
RoofSection17P={productions17}
RoofSection17S=
RoofSection17F=
RoofSection17N=
RoofSection18Box=1
RoofSection18E=
RoofSection18T={tilts18.Replace(".00 °", "")}
RoofSection18A={azimuths18.Replace(" °", "")}
RoofSection18M={modules18}
RoofSection18P={productions18}
RoofSection18S=
RoofSection18F=
RoofSection18N=
RoofSection19Box=1
RoofSection19E=
RoofSection19T={tilts19.Replace(".00 °", "")}
RoofSection19A={azimuths19.Replace(" °", "")}
RoofSection19M={modules19}
RoofSection19P={productions19}
RoofSection19S=
RoofSection19F=
RoofSection19N=
");
            string exeFolder = System.IO.Path.GetDirectoryName(Environment.CurrentDirectory);
            System.IO.File.WriteAllText(@"IEr6.9.ini", ini);

            // This will keep the console open until the user presses enter.
            // This allows the user to review the console dialogue to make sure something did not go wrong.
            // The only thing that would go wrong is that the PDF document intended to be read
            // Had its target text in the wrong place and the reader was now reading incorrect text.
            Console.WriteLine("Hotkey tool has been filled. Press enter to continue.");
            Console.ReadLine();
        }
    }
}
