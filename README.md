# PDF Reader for AHK .ini Auto Completion
This program was developed utilizing the iTextSharp Nuget package. The program's purpose was to read .pdf files and using a text location method and filter, put text from a .pdf into variables to fill out an .ini for an .ahk application.

The reason why is becasue some companies require employees to redundantly enter information that comes from one platform that isn't linked to another. This causes the employee to need to be a copy and paste operator.

Because this information always came from a .pdf that always followed the same template, it became clear that there might be a way to automate the data extraction. With the method created in .ahk, it was realized that .ahk applications can read .ini files to fill out variables in the .ahk application. The application has hotkeys programmed in to be able to let the user quickly enter this information across multiple platforms and applications. Making an employee faster at data entry.

# Tasks this program accomplishes:
- Reading a .pdf file.
- Converting found text into a unicode format.
- Stores converted text into variables.
- Fills out a .ini file to be used by a .ahk application.
- Writes the .ini file to hard drive as a .ini.

# Reflection
There is virtually no other platform that I know of that could be able to do this, except Java. This application was originally designed with the intent to autofill Excel sheets to save the user time. It was discovered later that filling out the .ini file used by the .ahk application was a better solution.

For the future, it seems that all application developments should be given the time to determine if maybe the solution attempted might be the wrong solution to achieve the same ends. This took months to come to the realization that there is a different way of going about a solution than intended. Were it not an individual project and had a team, it's possible the resultig application here would have been finished months earlier.